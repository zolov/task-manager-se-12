package ru.zolov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SessionUtil {

  private SessionUtil() {
  }

  private static String salt = "gilead";
  private static Integer cycle = 135;

  public static String sign(Object value) {
    @NotNull final ObjectMapper objectMapper = new ObjectMapper();
    try {
      @NotNull final String json = objectMapper.writeValueAsString(value);
      return sign(json, salt, cycle);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Nullable private static String sign(
      @Nullable final String value,
      @Nullable final String salt,
      @Nullable final Integer cycle
  ) {
    System.out.println(value);
    if (value == null || salt == null || cycle == null) return null;
    @Nullable String result = value;
    for (int i = 0; i < cycle; i++) {
      result = HashUtil.md5(salt + result + salt);
    }
    return result;
  }
}
