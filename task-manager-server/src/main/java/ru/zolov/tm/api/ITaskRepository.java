package ru.zolov.tm.api;

import java.sql.SQLException;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface ITaskRepository extends IRepository<Task> {

  @NotNull List<Task> findAllByProjectIdByUserId(
      @NotNull String userId,
      @NotNull String projectId
  ) throws SQLException, EmptyRepositoryException, EmptyStringException;

  @NotNull List<Task> findAllByUserId(@NotNull String userId) throws SQLException, EmptyStringException;

  void removeAllTaskByProjectId(
      @NotNull String userId,
      @NotNull String projectId
  ) throws EmptyRepositoryException, SQLException, EmptyStringException;

  @NotNull List<Task> findAllByProjId(
      @NotNull String projectId
  ) throws EmptyRepositoryException, SQLException, EmptyStringException;

  @NotNull List<Task> findTaskByPartOfTheName(
      @NotNull String userId,
      @NotNull String partOfTheName
  ) throws EmptyRepositoryException, SQLException, EmptyStringException;

  void removeAllByUserId(@NotNull String userId) throws SQLException;
}
