package ru.zolov.tm.api;

import java.sql.SQLException;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.Domain;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IDomainService {

  void load(@NotNull Domain domain) throws EmptyStringException, EmptyRepositoryException, SQLException;

  void save(@NotNull Domain domain) throws EmptyStringException, EmptyRepositoryException, SQLException;
}
