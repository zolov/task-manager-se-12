package ru.zolov.tm.api;

import java.sql.SQLException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;

@WebService
public interface IUserEndpoint {

  @NotNull @WebMethod User registerNewUser(
      @NotNull @WebParam(name = "login") String login,
      @NotNull @WebParam(name = "password") String password
  ) throws EmptyStringException, UserExistException, EmptyRepositoryException, SQLException;

  @NotNull @WebMethod User updateUserPassword(
      @NotNull @WebParam(name = "session") Session session,
      @NotNull @WebParam(name = "newPassword") String newPassword
  ) throws EmptyStringException, UserNotFoundException, AccessForbiddenException, CloneNotSupportedException, SQLException, EmptyRepositoryException;

  @WebMethod boolean isRolesAllowed(
      @NotNull @WebParam(name = "session") Session session,
      @Nullable @WebParam(name = "roleType") RoleType... roleTypes
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyRepositoryException, SQLException, EmptyStringException;
}
