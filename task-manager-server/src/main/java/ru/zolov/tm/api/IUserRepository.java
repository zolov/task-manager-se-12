package ru.zolov.tm.api;

import java.sql.SQLException;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IUserRepository extends IRepository<User> {

  User existsUser(
      @NotNull final String login,
      @NotNull final String passwordHash
  ) throws Exception;

  boolean findByLogin(final String login) throws EmptyStringException, EmptyRepositoryException, SQLException;
}
