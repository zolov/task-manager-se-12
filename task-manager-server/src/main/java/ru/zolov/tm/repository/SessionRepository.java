package ru.zolov.tm.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.enumerated.QueryConstant;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class SessionRepository extends AbstractRepository<Session> implements ru.zolov.tm.api.ISessionRepository {

  public SessionRepository(@Nullable Connection connection) {
    super(connection);
  }

  @Override @NotNull public List<Session> findAll() throws EmptyRepositoryException, SQLException, EmptyStringException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_session;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @NotNull final List<Session> resultList = new ArrayList<>();
    while (resultSet.next()) resultList.add(fetch(resultSet));
    statement.close();
    return resultList;
  }

  @Nullable @Override public Session findOne(final String id) throws EmptyStringException, EmptyRepositoryException, SQLException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_session WHERE id = ?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, id);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @Nullable Session session = new Session();
    while (resultSet.next()) session = fetch(resultSet);
    return session;
  }

  @Override public void persist(@NotNull final Session entity) throws SQLException, EmptyStringException {
    @NotNull final String query = "INSERT INTO tmdb_zolov.app_session (id, signature, user_id, timestamp) VALUES (?, ?, ?, ?);";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, entity.getId());
    statement.setString(2, entity.getSignature());
    statement.setString(3, entity.getUserId());
    statement.setLong(4, entity.getTimestamp());
    statement.execute();
  }

  @Override public void merge(@NotNull final Session entity) throws EmptyStringException, EmptyRepositoryException, SQLException {
    if (findOne(entity.getId()) == null) {
      persist(entity);
    }
    else {
      @NotNull final String query = "UPDATE tmdb_zolov.app_session SET signature = ?, timestamp = ?, user_id = ? WHERE id = ?;";
      if (getConnection() == null) throw new SQLException();
      @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
      statement.setString(1, entity.getSignature());
      statement.setLong(2, entity.getTimestamp());
      statement.setString(3, entity.getUserId());
      statement.execute();
    }
  }

  @Override public void remove(@NotNull final String id) throws SQLException {
    @NotNull final String query = "DELETE FROM tmdb_zolov.app_session WHERE id = ?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, id);
    statement.execute();
  }

  @Override public void removeAll() throws SQLException {
    @NotNull final String query = "DELETE FROM tmdb_zolov.app_session;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.execute();
  }

  @Override public void load(@NotNull final List<Session> list) throws SQLException, EmptyStringException, EmptyRepositoryException {
    for (@NotNull final Session session : list) {
      merge(session);
    }
  }

  @Nullable private Session fetch(@Nullable final ResultSet resultSet) throws SQLException, EmptyStringException {
    if (resultSet == null) return null;
    @NotNull final Session session = new Session();
    session.setId(resultSet.getString(QueryConstant.ID.name()));
    session.setSignature(resultSet.getString(QueryConstant.SIGNATURE.name()));
    session.setUserId(resultSet.getString(QueryConstant.USER_ID.name()));
    session.setTimestamp(resultSet.getLong(QueryConstant.TIMESTAMP.name()));
    return session;
  }
}
