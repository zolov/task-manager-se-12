package ru.zolov.tm.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.enumerated.QueryConstant;
import ru.zolov.tm.enumerated.StatusType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.util.FormatUtil;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

  public ProjectRepository(@Nullable Connection connection) {
    super(connection);
  }

  @Override @NotNull public List<Project> findProjectByPartOfTheName(
      @NotNull final String userId,
      @NotNull final String partOfTheName
  ) throws SQLException, EmptyStringException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_project WHERE user_id = '" + userId + "' AND '" + "(name LIKE '%'"
        + partOfTheName + "%' OR description LIKE '%" + partOfTheName + "%');";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @NotNull final List<Project> resultList = new ArrayList<>();
    while (resultSet.next()) resultList.add(fetch(resultSet));
    statement.close();
    return resultList;
  }

  @Override @Nullable public Project findOneByUserId(
      @NotNull final String userId,
      @NotNull final String id
  ) throws SQLException, EmptyStringException {
    @Nullable Project result = null;
    for (@NotNull final Project project : findAllByUserId(userId)) {
      if (project.getId().equals(id)) result = project;
    }
    return result;
  }

  @Override @NotNull public List<Project> findAllByUserId(@NotNull final String userId) throws SQLException, EmptyStringException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_project WHERE user_id = ?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, userId);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @NotNull final List<Project> resultList = new ArrayList<>();
    while (resultSet.next()) resultList.add(fetch(resultSet));
    statement.close();
    return resultList;
  }

  @Override public void removeAllByUserId(@NotNull final String userId) throws SQLException {
    @NotNull final String query = "DELETE FROM tmdb_zolov.app_project WHERE user_id = ?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, userId);
    statement.execute();
  }

  @NotNull public List<Project> findAll() throws SQLException, EmptyStringException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_project;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @NotNull final List<Project> resultList = new ArrayList<>();
    while (resultSet.next()) resultList.add(fetch(resultSet));
    statement.close();
    return resultList;
  }

  @Nullable public Project findOne(
      @NotNull final String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_project WHERE id = ?";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, id);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @Nullable Project project = null;
    while (resultSet.next()) project = fetch(resultSet);
    return project;
  }

  @Override public void persist(
      @NotNull final Project project
  ) throws SQLException, EmptyStringException {
    @NotNull String query = "INSERT INTO tmdb_zolov.app_project (id, date_create, date_start, date_finish,"
        + " description, name, status, user_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, project.getId());
    statement.setDate(2, FormatUtil.convertToSqlDate(project.getDateOfCreate()));
    statement.setDate(3, FormatUtil.convertToSqlDate(project.getDateOfStart()));
    statement.setDate(4, FormatUtil.convertToSqlDate(project.getDateOfFinish()));
    statement.setString(5, project.getDescription());
    statement.setString(6, project.getName());
    statement.setString(7, project.getStatus().toString());
    statement.setString(8, project.getUserId());
    statement.execute();
  }

  @Override public void merge(
      @NotNull final Project project
  ) throws EmptyStringException, EmptyRepositoryException, SQLException {
    if (getConnection() == null) throw new SQLException();
    if (findOne(project.getId()) == null) persist(project);
    @NotNull final String query = "UPDATE tmdb_zolov.app_project SET date_create = ?, date_start = ?, date_finish = ?,"
        + " description = ?, name = ?, status = ?, user_id = ? WHERE id = ?;";
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setDate(1, FormatUtil.convertToSqlDate(project.getDateOfCreate()));
    statement.setDate(2, FormatUtil.convertToSqlDate(project.getDateOfStart()));
    statement.setDate(3, FormatUtil.convertToSqlDate(project.getDateOfFinish()));
    statement.setString(4, project.getDescription());
    statement.setString(5, project.getName());
    statement.setString(6, project.getStatus().name());
    statement.setString(7, project.getUserId());
    statement.execute();
  }

  @Override public void remove(
      @NotNull final String id
  ) throws SQLException {
    @NotNull final String query = "DELETE FROM tmdb_zolov.app_project WHERE id = ?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, id);
    statement.execute();
  }

  @Override public void removeAll() throws SQLException {
    @NotNull final String query = "DELETE FROM tmdb_zolov.app_project;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.execute();
  }

  @Override public void load(
      @NotNull final List<Project> list
  ) throws SQLException, EmptyStringException, EmptyRepositoryException {
    for (
        @NotNull
        final Project project : list) { merge(project); }
  }

  @NotNull private Project fetch(@Nullable final ResultSet resultSet) throws SQLException, EmptyStringException {
    if (resultSet == null) return null;
    @NotNull final Project project = new Project();
    project.setName(resultSet.getString(QueryConstant.NAME.name()));
    project.setId(resultSet.getString(QueryConstant.ID.name()));
    project.setUserId(resultSet.getString(QueryConstant.USER_ID.name()));
    project.setDescription(resultSet.getString(QueryConstant.DESCRIPTION.name()));
    project.setDateOfCreate(FormatUtil.convertToDate(resultSet.getDate(QueryConstant.DATE_CREATE.name())));
    project.setDateOfStart(FormatUtil.convertToDate(resultSet.getDate(QueryConstant.DATE_START.name())));
    project.setDateOfFinish(FormatUtil.convertToDate(resultSet.getDate(QueryConstant.DATE_FINISH.name())));
    project.setStatus(StatusType.valueOf(resultSet.getString(QueryConstant.STATUS.name())));
    return project;
  }
}
