package ru.zolov.tm.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IUserRepository;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.QueryConstant;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserNotFoundException;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

  public UserRepository(final @Nullable Connection connection) {
    super(connection);
  }

  @Override public User existsUser(
      @NotNull final String login,
      @NotNull final String passwordHash
  ) throws UserNotFoundException, SQLException {
    @NotNull final String query = "SELECT * FROM app_user WHERE login = ? AND password_hash = ?";
    @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
    if (preparedStatement == null) throw new SQLException();
    preparedStatement.setString(1, login);
    preparedStatement.setString(2, passwordHash);
    @Nullable ResultSet resultSet = preparedStatement.executeQuery();
    if (resultSet == null) throw new UserNotFoundException();
    @NotNull User user = new User();
    while (resultSet.next()) user = fetch(resultSet);
    return user;
  }

  @Override public boolean findByLogin(final String login) throws EmptyStringException, EmptyRepositoryException, SQLException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_user WHERE login = ?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, login);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @Nullable User user = new User();
    while (resultSet.next()) user = fetch(resultSet);
    return login.equals(user.getLogin());
  }

  @SneakyThrows @Override public @NotNull List<User> findAll() throws EmptyRepositoryException, EmptyStringException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_user;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @NotNull final List<User> resultList = new ArrayList<>();
    while (resultSet.next()) resultList.add(fetch(resultSet));
    statement.close();
    return resultList;
  }

  @Nullable @Override public User findOne(final String id) throws EmptyStringException, EmptyRepositoryException, SQLException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_user WHERE id = ?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, id);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @Nullable User user = new User();
    while (resultSet.next()) user = fetch(resultSet);
    return user;
  }

  @Override public void persist(@NotNull final User entity) throws SQLException, EmptyStringException {
    @NotNull final String query = "INSERT INTO tmdb_zolov.app_user (id, login, password_hash, role) VALUES ( ?, ?, ?, ?);";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, entity.getId());
    statement.setString(2, entity.getLogin());
    statement.setString(3, entity.getPasswordHash());
    statement.setString(4, entity.getRole().getDisplayName());
    statement.execute();
  }

  @Override public void merge(@NotNull final User entity) throws EmptyStringException, EmptyRepositoryException, SQLException {
    if (findOne(entity.getId()) == null) {
      persist(entity);
    }
    else {
      @NotNull final String query = "UPDATE tmdb_zolov.app_user SET login = ?, password_hash = ?, role = ? WHERE id = ?;";
      if (getConnection() == null) throw new SQLException();
      @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
      statement.setString(1, entity.getLogin());
      statement.setString(2, entity.getPasswordHash());
      statement.setString(3, entity.getRole().name());
      statement.setString(4, entity.getId());
      statement.execute();
    }
  }

  @Override public void remove(@NotNull final String id) throws SQLException {
    @NotNull final String query = "DELETE FROM tmdb_zolov.app_user WHERE id=?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, id);
    statement.execute();

  }

  @Override public void removeAll() throws SQLException {
    @NotNull final String query = "DELETE FROM tmdb_zolov.app_user;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.execute();

  }

  @Override public void load(@NotNull final List<User> list) throws SQLException, EmptyStringException, EmptyRepositoryException {
    for (@NotNull final User user : list) {
      merge(user);
    }
  }

  @Nullable private User fetch(@Nullable final ResultSet resultSet) throws SQLException {
    if (resultSet == null) return null;
    @Nullable final User user = new User();
    user.setId(resultSet.getString(QueryConstant.ID.name()));
    user.setLogin(resultSet.getString(QueryConstant.LOGIN.name()));
    user.setPasswordHash(resultSet.getString(QueryConstant.PASSWORD_HASH.name()));
    user.setRole(RoleType.valueOf(resultSet.getString(QueryConstant.ROLE.name())));
    return user;
  }
}
