package ru.zolov.tm.repository;

import java.sql.Connection;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IRepository;
import ru.zolov.tm.entity.AbstractEntity;

@NoArgsConstructor
@Getter
abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

  @Nullable
  Connection connection;

  public AbstractRepository(@Nullable final Connection connection) {
    this.connection = connection;
  }

}
