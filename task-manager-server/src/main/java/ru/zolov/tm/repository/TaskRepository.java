package ru.zolov.tm.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.enumerated.QueryConstant;
import ru.zolov.tm.enumerated.StatusType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.util.FormatUtil;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

  public TaskRepository(final @Nullable Connection connection) {
    super(connection);
  }

  @Override @NotNull public List<Task> findAllByProjectIdByUserId(
      @NotNull final String userId,
      @NotNull final String projectId
  ) throws SQLException, EmptyRepositoryException, EmptyStringException {
    @NotNull final List<Task> resultListByUserId = new ArrayList<>();
    for (@NotNull final Task task : findAllByProjId(projectId)) {
      if (task.getUserId().equals(userId)) resultListByUserId.add(task);
    }
    return resultListByUserId;
  }

  @Override @NotNull public List<Task> findAllByUserId(@NotNull final String userId) throws SQLException, EmptyStringException {
    @NotNull final String query = "SELECT FROM tmdb_zolov.app_task WHERE user_id = ?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, userId);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @NotNull final List<Task> resultList = new ArrayList<>();
    while (resultSet.next()) resultList.add(fetch(resultSet));
    statement.close();
    return resultList;

  }

  @Override public void removeAllTaskByProjectId(
      @NotNull final String userId,
      @NotNull final String projectId
  ) throws EmptyRepositoryException, SQLException, EmptyStringException {
    @NotNull final List<Task> taskToDelete = findAllByProjectIdByUserId(userId, projectId);
    for (@NotNull final Task task : taskToDelete) {
      remove(task.getId());
    }
  }

  @Override @NotNull public List<Task> findAllByProjId(
      @NotNull final String projectId
  ) throws EmptyRepositoryException, SQLException, EmptyStringException {
    @NotNull final List<Task> resultList = new ArrayList<>();
    for (@NotNull final Task task : findAll()) {
      if (task.getProjectId().equals(projectId)) resultList.add(task);
    }
    return resultList;
  }

  @Override @NotNull public List<Task> findTaskByPartOfTheName(
      @NotNull final String userId,
      @NotNull final String partOfTheName
  ) throws EmptyRepositoryException, SQLException, EmptyStringException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_task WHERE user_id = '" + userId + "' AND (name LIKE '%" + partOfTheName
        + "%' OR description LIKE '%" + partOfTheName + "%');";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @NotNull final List<Task> resultList = new ArrayList<>();
    while (resultSet.next()) resultList.add(fetch(resultSet));
    statement.close();
    return resultList;
  }

  @Override public void removeAllByUserId(@NotNull final String userId) throws SQLException {
    @NotNull final String query = "DELETE FROM tmdb_zolov.app_task WHERE user_id = ?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, userId);
    statement.execute();
  }

  @NotNull @Override public List<Task> findAll() throws EmptyRepositoryException, SQLException, EmptyStringException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_task;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @NotNull final List<Task> resultList = new ArrayList<>();
    while (resultSet.next()) resultList.add(fetch(resultSet));
    statement.close();
    return resultList;
  }

  @Nullable @Override public Task findOne(final String id) throws EmptyStringException, EmptyRepositoryException, SQLException {
    @NotNull final String query = "SELECT * FROM tmdb_zolov.app_task WHERE id = ?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, id);
    @NotNull final ResultSet resultSet = statement.executeQuery();
    @NotNull Task task = null;
    while (resultSet.next()) task = fetch(resultSet);
    statement.close();
    return task;
  }


  @Override public void persist(@NotNull final Task task) throws SQLException, EmptyStringException {
    @NotNull final String query = "INSERT INTO tmdb_zolov.app_task (id, date_create, date_start, date_finish,"
        + " description, name, status, user_id, project_id) VALUES = (?, ?, ?, ?, ?, ?, ?, ?, ?);";
    if (getConnection() == null) throw new SQLException();
    final PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, task.getId());
    statement.setDate(2, FormatUtil.convertToSqlDate(task.getDateOfCreate()));
    statement.setDate(3, FormatUtil.convertToSqlDate(task.getDateOfStart()));
    statement.setDate(4, FormatUtil.convertToSqlDate(task.getDateOfFinish()));
    statement.setString(5, task.getDescription());
    statement.setString(6, task.getName());
    statement.setString(7, task.getStatus().name());
    statement.setString(8, task.getUserId());
    statement.setString(9, task.getProjectId());
    statement.execute();
  }

  @Override public void merge(@NotNull final Task task) throws SQLException, EmptyStringException, EmptyRepositoryException {
    if (findOne(task.getId()) == null) {
      persist(task);
    }
    else {
      @NotNull final String query = "UPDATE tmdb_zolov.app_task SET date_create = ?, date_start = ?, date_finish =?,"
          + " description = ?, name = ?, status = ?, user_id = ?, project_id = ? WHERE id = ?;";
      if (getConnection() == null) throw new SQLException();
      final PreparedStatement statement = getConnection().prepareStatement(query);
      statement.setDate(1, FormatUtil.convertToSqlDate(task.getDateOfCreate()));
      statement.setDate(2, FormatUtil.convertToSqlDate(task.getDateOfStart()));
      statement.setDate(3, FormatUtil.convertToSqlDate(task.getDateOfFinish()));
      statement.setString(4, task.getDescription());
      statement.setString(5, task.getName());
      statement.setString(6, task.getStatus().name());
      statement.setString(7, task.getUserId());
      statement.setString(8, task.getProjectId());
      statement.setString(9, task.getId());
      statement.execute();
    }
  }

  @Override public void remove(@NotNull final String id) throws SQLException {
    @NotNull final String query = "DELETE FROM tmdb_zolov.app_task WHERE id = ?;";
    if (getConnection() == null) throw new SQLException();
    @NotNull PreparedStatement statement = getConnection().prepareStatement(query);
    statement.setString(1, id);
    statement.execute();
  }

  @Override public void removeAll() throws SQLException {
    @NotNull final String query = "DELETE FROM tmdb_zolov.app_task;";
    if (getConnection() == null) throw new SQLException();
    @NotNull PreparedStatement statement = getConnection().prepareStatement(query);
    statement.execute();
  }

  @Override public void load(@NotNull List<Task> list) throws EmptyRepositoryException, SQLException, EmptyStringException {
    for (@NotNull final Task task : list) {
      merge(task);
    }
  }

  @Nullable private Task fetch(@Nullable final ResultSet resultSet) throws SQLException, EmptyStringException {
    if (resultSet == null) return null;
    @NotNull final Task task = new Task();
    task.setId(resultSet.getString(QueryConstant.ID.name()));
    task.setUserId(resultSet.getString(QueryConstant.USER_ID.name()));
    task.setProjectId(resultSet.getString(QueryConstant.PROJECT_ID.name()));
    task.setDescription(resultSet.getString(QueryConstant.DESCRIPTION.name()));
    task.setName(resultSet.getString(QueryConstant.NAME.name()));
    task.setStatus(StatusType.valueOf(resultSet.getString(QueryConstant.STATUS.name())));
    task.setDateOfCreate(resultSet.getDate(QueryConstant.DATE_CREATE.name()));
    task.setDateOfStart(resultSet.getDate(QueryConstant.DATE_START.name()));
    task.setDateOfFinish(resultSet.getDate(QueryConstant.DATE_FINISH.name()));
    return task;
  }
}


