package ru.zolov.tm.comparator;

import java.util.Comparator;
import lombok.SneakyThrows;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.exception.EmptyRepositoryException;

public class DateStartComparator implements Comparator<AbstractGoal> {

  @SneakyThrows
  @Override
  public int compare(
      AbstractGoal a1,
      AbstractGoal a2
  ) {
    if (a1 == null || a2 == null) throw new EmptyRepositoryException();
    return a1.getDateOfStart().compareTo(a2.getDateOfStart());
  }

}
