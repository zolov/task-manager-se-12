package ru.zolov.tm.endpoint;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Comparator;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IProjectEndpoint;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;


@NoArgsConstructor
@WebService(endpointInterface = "ru.zolov.tm.api.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

  @NotNull @Override @WebMethod public Project createNewProject(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "name") final String projectName,
      @NotNull @WebParam(name = "description") final String projectDescription,
      @NotNull @WebParam(name = "start") final String dateOfStart,
      @NotNull @WebParam(name = "finish") final String dateOfFinish
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, ParseException, SQLException, EmptyRepositoryException {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getProjectService().create(session.getUserId(), projectName, projectDescription, dateOfStart, dateOfFinish);
  }

  @NotNull @Override @WebMethod public List<Project> findAllProject(
      @NotNull @WebParam(name = "session") final Session session
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException {
    serviceLocator.getSessionService().validate(session);
    if (session.getUserId() == null) throw new AccessForbiddenException();
    return serviceLocator.getProjectService().readAll();
  }

  @NotNull @Override @WebMethod public List<Project> findAllProjectByUserId(
      @NotNull @WebParam(name = "session") final Session session
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getProjectService().readAll(session.getUserId());
  }

  @Nullable @Override @WebMethod public Project findProjectById(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "id") String projectId
  ) throws EmptyStringException, EmptyRepositoryException, SQLException {
    return serviceLocator.getProjectService().read(session.getUserId(), projectId);
  }

  @Override @WebMethod public void removeProjectById(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "id") String projectId
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getProjectService().remove(session.getUserId(), projectId);
  }

  @Override @WebMethod public void updateProject(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "id") final String projectId,
      @NotNull @WebParam(name = "name") final String projectName,
      @NotNull @WebParam(name = "description") final String projectDescription,
      @NotNull @WebParam(name = "start") final String start,
      @NotNull @WebParam(name = "finish") final String finish
  ) throws AccessForbiddenException, CloneNotSupportedException, ParseException, EmptyRepositoryException, SQLException, EmptyStringException {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getProjectService().update(session.getUserId(), projectId, projectName, projectDescription, start, finish);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Project> getSortedProjectList(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "comparator") final String comparatorName
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyRepositoryException, EmptyStringException {
    serviceLocator.getSessionService().validate(session);
    final Comparator<AbstractGoal> comparator = serviceLocator.getTerminalService().getComparator(comparatorName);
    return serviceLocator.getProjectService().sortBy(session.getUserId(), comparator);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Project> findProjectByPartOfTheName(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "partOfTheName") final String partOfTheName
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyRepositoryException, EmptyStringException {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getProjectService().findProject(session.getUserId(), partOfTheName);
  }
}
