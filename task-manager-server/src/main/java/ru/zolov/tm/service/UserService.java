package ru.zolov.tm.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;
import ru.zolov.tm.repository.UserRepository;
import ru.zolov.tm.util.ConnectionUtil;
import ru.zolov.tm.util.HashUtil;

public class UserService extends AbstractService<User> implements IUserService {

  @Override public User login(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException, UserNotFoundException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final UserRepository userRepository = new UserRepository(connection);
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @Nullable User user = null;
    final String passwordHash = HashUtil.md5(password);
    if (passwordHash == null) throw new UserNotFoundException();
    user = userRepository.existsUser(login, passwordHash);
    if (user == null) throw new UserNotFoundException();
    return user;
  }

  @NotNull @Override public User userRegistration(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException, UserExistException, SQLException, EmptyRepositoryException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final UserRepository userRepository = new UserRepository(connection);
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    if (userRepository.findByLogin(login)) throw new UserExistException();
    final User newUser = new User();
    newUser.setLogin(login);
    newUser.setRole(RoleType.USER);
    @Nullable String hashdPassword = HashUtil.md5(password);
    if (hashdPassword == null) throw new EmptyStringException();
    newUser.setPasswordHash(hashdPassword);
    userRepository.persist(newUser);
    return newUser;
  }

  @Override public void adminRegistration(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException, SQLException, EmptyRepositoryException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final UserRepository userRepository = new UserRepository(connection);
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    if (userRepository.findByLogin(login)) throw new EmptyStringException();
    @Nullable final User admin = new User();
    admin.setLogin(login);
    admin.setRole(RoleType.ADMIN);
    admin.setPasswordHash(HashUtil.md5(password));
    userRepository.persist(admin);
  }

  @NotNull public List<User> readAll() throws EmptyRepositoryException, EmptyStringException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final UserRepository userRepository = new UserRepository(connection);
    List<User> list = new ArrayList<>();
    list = userRepository.findAll();
    return list;
  }

  @Override public void remove(@Nullable final String id) throws EmptyStringException, SQLException {
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final UserRepository userRepository = new UserRepository(connection);
    userRepository.remove(id);
  }

  @NotNull @Override public User updateUserPassword(
      @Nullable final String id,
      @Nullable final String password
  ) throws EmptyStringException, UserNotFoundException, EmptyRepositoryException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final UserRepository userRepository = new UserRepository(connection);
    @Nullable User user = null;
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    user = userRepository.findOne(id);
    user.setPasswordHash(HashUtil.md5(password));
    userRepository.merge(user);
    return user;
  }

  @SneakyThrows @NotNull @Override public User updateUserProfile(
      @Nullable String id,
      @Nullable String name,
      @Nullable String password
  ) throws EmptyStringException, UserNotFoundException, EmptyRepositoryException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final UserRepository userRepository = new UserRepository(connection);
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @Nullable User user = null;
    user = userRepository.findOne(id);
    user.setPasswordHash(HashUtil.md5(password));
    userRepository.merge(user);
    user.setLogin(name);
    return user;
  }

  @Override public void load(@Nullable List<User> list) throws EmptyRepositoryException, EmptyStringException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final UserRepository userRepository = new UserRepository(connection);
    if (list == null) throw new EmptyRepositoryException();
    userRepository.load(list);
  }
}
