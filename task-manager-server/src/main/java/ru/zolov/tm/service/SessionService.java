package ru.zolov.tm.service;


import java.sql.Connection;
import java.sql.SQLException;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ISessionRepository;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.repository.SessionRepository;
import ru.zolov.tm.util.ConnectionUtil;
import ru.zolov.tm.util.SessionUtil;

public class SessionService extends AbstractService<Session> implements ISessionService {

  @NotNull @Override public Session open(final User user) throws EmptyStringException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    final ISessionRepository sessionRepository = new SessionRepository(connection);
    final Session session = new Session();
    session.setUserId(user.getId());
    session.setRoleType(user.getRole());
    session.setTimestamp(System.currentTimeMillis());
    session.setSignature(SessionUtil.sign(session));
    sessionRepository.persist(session);
    return session;
  }

  @Override public void validate(Session session) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    final ISessionRepository sessionRepository = new SessionRepository(connection);
    if (session == null) throw new AccessForbiddenException();
    if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
    if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessForbiddenException();
    final Session temp = session.clone();
    if (temp == null) throw new AccessForbiddenException();
    final String signatureSource = session.getSignature();
    temp.setSignature(null);
    final String signatureTarget = SessionUtil.sign(temp);
    final boolean check = signatureSource.equals(signatureTarget);
    if (!check) throw new AccessForbiddenException();
    if (sessionRepository.findOne(session.getId()) == null) throw new AccessForbiddenException();
  }

  @Override public void close(Session session) throws SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    final ISessionRepository sessionRepository = new SessionRepository(connection);
    sessionRepository.remove(session.getId());
  }
}
