package ru.zolov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IDomainService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.entity.Domain;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class DomainService implements IDomainService {

  protected ServiceLocator serviceLocator;

  public DomainService(ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  @SneakyThrows @Override public void load(@Nullable final Domain domain) throws EmptyStringException, EmptyRepositoryException {
    if (domain == null) return;
    serviceLocator.getProjectService().load(domain.getProjects());
    serviceLocator.getTaskService().load(domain.getTasks());
    serviceLocator.getUserService().load(domain.getUsers());

  }

  @SneakyThrows @Override public void save(@Nullable final Domain domain) throws EmptyStringException, EmptyRepositoryException {
    if (domain == null) return;
    domain.setProjects(serviceLocator.getProjectService().readAll());
    domain.setTasks(serviceLocator.getTaskService().readAll());
    domain.setUsers(serviceLocator.getUserService().readAll());
  }
}
