package ru.zolov.tm.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.repository.TaskRepository;
import ru.zolov.tm.util.ConnectionUtil;

public class TaskService extends AbstractService<Task> implements ITaskService {

  @NotNull public Task create(
      @Nullable final String userId,
      @Nullable final String projectId,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable final String start,
      @Nullable final String finish
  ) throws EmptyStringException, ParseException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final TaskRepository taskRepository = new TaskRepository(connection);
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    if (projectId == null || projectId.isEmpty()) throw new EmptyStringException();
    if (start == null || start.isEmpty()) throw new EmptyStringException();
    if (finish == null || finish.isEmpty()) throw new EmptyStringException();
    Task task = new Task();
    task.setUserId(userId);
    task.setProjectId(projectId);
    task.setName(name);
    task.setDescription(description);
    task.setDateOfStart(dateFormat.parse(start));
    task.setDateOfFinish(dateFormat.parse(finish));
    taskRepository.persist(task);
    return task;
  }

  @NotNull public List<Task> readAll(@Nullable String userId) throws EmptyRepositoryException, EmptyStringException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final TaskRepository taskRepository = new TaskRepository(connection);
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    @NotNull List<Task> list = new ArrayList<>();
    list = taskRepository.findAll();
    return list;
  }

  @NotNull public List<Task> readAll() throws EmptyRepositoryException, EmptyStringException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final TaskRepository taskRepository = new TaskRepository(connection);
    @NotNull List<Task> list = new ArrayList<>();
    list = taskRepository.findAll();
    return list;
  }

  public List<Task> readTaskByProjectId(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final TaskRepository taskRepository = new TaskRepository(connection);
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    @NotNull List<Task> list = new ArrayList<>();
    list = taskRepository.findAllByProjectIdByUserId(userId, id);
    return list;
  }

  @Nullable public Task readTaskById(
      @Nullable final String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final TaskRepository taskRepository = new TaskRepository(connection);
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    @Nullable Task task = null;
    task = taskRepository.findOne(id);
    return task;
  }

  public void remove(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final TaskRepository taskRepository = new TaskRepository(connection);
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    taskRepository.remove(id);
  }


  public void update(
      @Nullable final String userId,
      @Nullable final String id,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable final String start,
      @Nullable final String finish
  ) throws EmptyStringException, ParseException, EmptyRepositoryException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final TaskRepository taskRepository = new TaskRepository(connection);
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    @NotNull final Task task = new Task();
    task.setId(id);
    task.setUserId(userId);
    task.setName(name);
    task.setDescription(description);
    @NotNull final Date startDate = dateFormat.parse(start);
    task.setDateOfStart(startDate);
    @NotNull final Date finishDate = dateFormat.parse(finish);
    task.setDateOfFinish(finishDate);
    taskRepository.merge(task);
  }

  @NotNull public List<Task> sortBy(
      @Nullable final String userId,
      @Nullable final String projectId,
      @Nullable Comparator<AbstractGoal> comparator
  ) throws EmptyStringException, EmptyRepositoryException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final TaskRepository taskRepository = new TaskRepository(connection);
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (projectId == null || projectId.isEmpty()) throw new EmptyStringException();
    List<Task> list = new ArrayList<>();
    list = taskRepository.findAllByProjectIdByUserId(userId, projectId);
    Collections.sort(list, comparator);
    return list;
  }

  @NotNull public List<Task> findTask(
      @Nullable final String userId,
      @Nullable final String partOfTheName
  ) throws EmptyRepositoryException, EmptyStringException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final TaskRepository taskRepository = new TaskRepository(connection);
    List<Task> list = new ArrayList<>();
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (partOfTheName == null) throw new EmptyStringException();
    list = taskRepository.findTaskByPartOfTheName(userId, partOfTheName);
    return list;
  }

  public void load(@Nullable List<Task> list) throws EmptyRepositoryException, SQLException, EmptyStringException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @Nullable final TaskRepository taskRepository = new TaskRepository(connection);
    if (list == null) throw new EmptyRepositoryException();
    taskRepository.load(list);
  }
}
