package ru.zolov.tm.command.user;

import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.ParseException_Exception;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.SQLException_Exception;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.api.UserExistException_Exception;
import ru.zolov.tm.api.UserNotFoundException_Exception;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.util.TerminalUtil;

public class UserAuthorizationCommand extends AbstractCommand {

  private final String name = "user-auth";
  private final String description = "Auth user";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return true;
  }

  @Override public void execute() throws EmptyRepositoryException_Exception, UserExistException_Exception, EmptyStringException_Exception, UserNotFoundException_Exception, ParseException_Exception, SQLException_Exception {
    System.out.println("Please enter login: ");
    final String login = TerminalUtil.nextLine();
    System.out.println("Enter password: ");
    final String password = TerminalUtil.nextLine();
    Session session = bootstrap.getSessionEndpoint().openSession(login, password);
    if (session == null) throw new UserNotFoundException_Exception();
    bootstrap.setCurrentSession(session);
    System.out.println("Create new session for " + session.getUserId());
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
