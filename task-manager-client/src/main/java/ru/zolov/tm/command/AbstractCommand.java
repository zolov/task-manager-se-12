package ru.zolov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.loader.Bootstrap;

public abstract class AbstractCommand implements Comparable<AbstractCommand> {

  protected Bootstrap bootstrap;

  public void setBootstrap(Bootstrap bootstrap) {
    this.bootstrap = bootstrap;
  }

  public abstract String getName();

  public abstract String getDescription();

  public abstract boolean secure();

  public abstract void execute() throws Exception;

  public abstract RoleType[] roles();

  @Override
  public int compareTo(@NotNull AbstractCommand abstractCommand) {
    return this.getName().compareTo(abstractCommand.getName());
  }
}
