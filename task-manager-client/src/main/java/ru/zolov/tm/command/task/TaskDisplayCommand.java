package ru.zolov.tm.command.task;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.Project;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.SQLException_Exception;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.api.Task;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.util.TerminalUtil;


public final class TaskDisplayCommand extends AbstractCommand {

  private final String name = "task-list";
  private final String description = "Display task of list";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return false;
  }

  @Override public void execute() throws EmptyStringException_Exception, CloneNotSupportedException_Exception, AccessForbiddenException_Exception, EmptyRepositoryException_Exception, SQLException_Exception {
    @Nullable final Session session = bootstrap.getCurrentSession();
    if (session == null) return;
    final List<Project> listOfProjects = bootstrap.getProjectEndpoint().findAllProject(session);
    for (Project project : listOfProjects) {
      System.out.println("________________________________________");
      System.out.println(String.format(
          "%n Project: %s " + "%n Project ID: %s " + "%n Project description: %s " + "%n Status: %s " + "%n Date of create: %s "
              + "%n Date of start: %s " + "%n Date of finish: %s", project.getName(), project.getId(), project.getDescription(), project
              .getStatus(), project.getDateOfCreate(), project.getDateOfStart(), project.getDateOfFinish()));
    }
    System.out.print("Enter project id: ");
    @NotNull final String projectId = TerminalUtil.nextLine();
    System.out.printf("Choose sorting type: %n status %n date-create %n date-start %n date-finish");
    @NotNull final String comparatorName = TerminalUtil.nextLine();
    @NotNull final List<Task> listOfTask = bootstrap.getTaskEndpoint().getSortedTaskList(session, projectId, comparatorName);
    for (Task task : listOfTask) {
      System.out.println("________________________________________");
      System.out.println(String.format(
          "%n Task: %s " + "%n Task ID: %s " + "%n Task description: %s " + "%n Status: %s " + "%n Date of create: %s "
              + "%n Date of start: %s " + "%n Date of finish: %s", task.getName(), task.getId(), task.getDescription(), task
              .getStatus(), task.getDateOfCreate(), task.getDateOfStart(), task.getDateOfFinish()));
    }
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
