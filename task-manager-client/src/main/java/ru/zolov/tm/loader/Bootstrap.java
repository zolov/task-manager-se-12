package ru.zolov.tm.loader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.IDomainEndpoint;
import ru.zolov.tm.api.IProjectEndpoint;
import ru.zolov.tm.api.ISessionEndpoint;
import ru.zolov.tm.api.ITaskEndpoint;
import ru.zolov.tm.api.IUserEndpoint;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.endpoint.DomainEndpointService;
import ru.zolov.tm.endpoint.ProjectEndpointService;
import ru.zolov.tm.endpoint.SessionEndpointService;
import ru.zolov.tm.endpoint.TaskEndpointService;
import ru.zolov.tm.endpoint.UserEndpointService;
import ru.zolov.tm.util.TerminalUtil;

public final class Bootstrap {

  private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.zolov.tm").getSubTypesOf(AbstractCommand.class);
  private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
  private Session session = null;
  private IDomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();
  private ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
  private IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
  private IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
  private ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

  public @Nullable Session getCurrentSession() {
    return session;
  }

  public void setCurrentSession(@NotNull final Session session) throws EmptyRepositoryException_Exception {
    this.session = session;
  }

  public @NotNull IDomainEndpoint getDomainEndpoint() {
    return domainEndpoint;
  }

  public @NotNull ISessionEndpoint getSessionEndpoint() {
    return sessionEndpoint;
  }

  public @NotNull IUserEndpoint getUserEndpoint() {
    return userEndpoint;
  }

  public @NotNull IProjectEndpoint getProjectEndpoint() {
    return projectEndpoint;
  }

  public @NotNull ITaskEndpoint getTaskEndpoint() {
    return taskEndpoint;
  }

  public @NotNull List<AbstractCommand> getCommandList() {
    List<AbstractCommand> commandsList = new ArrayList<>(this.commands.values());
    Collections.sort(commandsList);
    return commandsList;
  }

  private void registry(@NotNull final Set<Class<? extends AbstractCommand>> classes) throws Exception {
    for (@NotNull final Class clazz : classes) {
      registry(clazz);
    }
  }

  private void registry(@NotNull final Class clazz) throws Exception {
    if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
    Object command = clazz.getDeclaredConstructor().newInstance();
    AbstractCommand abstractCommand = (AbstractCommand)command;
    registry(abstractCommand);
  }

  private void registry(AbstractCommand command) throws EmptyStringException_Exception {
    @NotNull final String cliCommand = command.getName();
    @NotNull final String cliDescription = command.getDescription();
    if (cliCommand == null || cliCommand.isEmpty()) throw new EmptyStringException_Exception();
    if (cliDescription == null || cliDescription.isEmpty()) throw new EmptyStringException_Exception();
    command.setBootstrap(this);
    commands.put(cliCommand, command);
  }

  public void init() throws Exception {
    if (classes == null) throw new EmptyRepositoryException_Exception();
    BasicConfigurator.configure();
    registry(classes);
    start();
  }

  private void start() {
    drawHeader();
    String command = "";
    while (true) {
      try {
        System.out.print("Enter command: \n");
        command = TerminalUtil.nextLine();
        execute(command);
      } catch (Exception e) {
        System.err.println(e.getMessage());
        e.printStackTrace();
      }
    }
  }

  private void execute(String command) throws Exception {
    final AbstractCommand abstractCommand = commands.get(command);
    if (abstractCommand == null) return;
    final boolean rolesAllowed = true;
    final boolean secureCheck = abstractCommand.secure() || (!abstractCommand.secure() && rolesAllowed);
    if (secureCheck) abstractCommand.execute();
    else System.out.println("Error! Access forbidden for this command");
  }

  private void drawHeader() {

    System.out.println("+----------------------------------------------------------------------------------------------------------+");
    System.out.println("|                Welcome to the task manager. Enter help to display a list of commands.                    |");
    System.out.println("+----------------------------------------------------------------------------------------------------------+");
  }
}